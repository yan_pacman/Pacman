    import {pointDraw,findPath} from './ProceduralMap.js'
    
        const canvas = document.querySelector('#canvas')
        const ctx = canvas.getContext('2d')
            
        document.addEventListener("keydown", keyController)
        
        ctx.canvas.width=window.innerWidth;
        ctx.canvas.height=window.innerHeight;

        console.log(window.innerHeight) //969
        console.log(window.innerWidth) //1365
        
        let x=100
        let y=100
        let width=50
        let height=20
        
        let pacman = class {
            constructor(x,y)
          {this.x=x
          this.y=y
          this.size=30
          this.height=30
          this.width=30
          this.direction=""} //empty, so that it doesnt move until the player wants it to!
          
          draw(){
          ctx.fillstyle='yellow'
          ctx.beginPath()
          ctx.arc(p.x,p.y, p.size, 0, 2 * Math.PI)
          ctx.fill()
          }
        }
        
        function drawPac(){
        ctx.clearRect(0,0,window.innerWidth,window.innerHeight)
        ctx.fillStyle='rgb(255,255,0)'
        ctx.beginPath();
        ctx.arc(p.x,p.y,p.size,0,2*Math.PI)
        ctx.fill()
        }
        
        let p=new pacman(x,y)
        
        let ghost = class {
        constructor(x,y)
          {this.x=x
          this.y=y
          this.size=15
          this.direction="U"}
          
        changeDirectionToRandom(){
        let random=Math.floor(Math.random()*4)+1
        if(random==1) this.direction="L"
        if(random==2) this.direction="R"
        if(random==3) this.direction="U"
        if(random==4) this.direction="D"
        }
        draw(){
            ctx.fillstyle=`hsl(${this.color},100%,50%)`
            ctx.beginPath()
            ctx.arc(this.x,this.y, this.size, 0, 2 * Math.PI)
            ctx.fill()}
        }
        
        let ghosts=[]
        ghosts.push(new ghost(500,500))
        ghosts.push(new ghost(750,750))
        ghosts.push(new ghost(1000,1000))
        ghosts.push(new ghost(1250,1250))
        
        function drawGhosts(){
        for (ghost of ghosts) {ghost.draw()}
        }
        
        function moveGhosts(speed){
              if (ghost.direction=="L")ghost.x-=speed/3
              if (ghost.direction=="R")ghost.x+=speed/3
              if (ghost.direction=="U")ghost.y-=speed/3
              if (ghost.direction=="D")ghost.y+=speed/3
        }

        function keyController(e){
        console.log(e.code)
        if(e.code=="KeyD"){p.direction="R"}
        if(e.code=="KeyA"){p.direction="L"}
        if(e.code=="KeyW"){p.direction="U"}
        if(e.code=="KeyS"){p.direction="D"}
        }
    
        function randomiseMovement(){
        for (ghost of ghosts){
        ghost.changeDirectionToRandom();
        }
        }
        
        function movePac(){
          if (p.direction=="L") {p.x-=2}
          if (p.direction=="R") {p.x+=2}
          if (p.direction=="U") {p.y-=2}
          if (p.direction=="D") {p.y+=2} 
          }

        function draw(){
        ctx.clearRect(0,0,window.innerWidth,window.innerHeight)
        
        //movePac()
        //Collide()
       // drawPac()
        
        //drawGhosts()
        //moveGhosts(1)

         
        //drawWalls()

        pointDraw()
        }

        window.Intervals = function(){
        findPath()
        setInterval(draw,100)
        //setInterval(randomiseMovement,1000) //set intervvalite trqbva da sa vutre vuv funktiona koito vikam v html :p
        }      